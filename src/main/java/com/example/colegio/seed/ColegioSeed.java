/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.colegio.seed;

import com.example.colegio.models.service.IColegioService;
import com.example.colegio.models.entity.Colegio;
import com.example.colegio.models.entity.Curso;
import com.example.colegio.models.entity.Asignatura;
import com.example.colegio.models.entity.Estudiante;
import com.example.colegio.models.entity.Profesor;
import com.example.colegio.models.service.ProfesorServiceImpl;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 *
 * @author ADMIN
 */

@Order(2)
@Component
public class ColegioSeed implements CommandLineRunner{
    
    @Autowired
    private IColegioService colegioService;
    
    @Autowired
    private ProfesorServiceImpl profesorService; 
        
    @Override
    public void run(String... args) throws Exception {
        loadColegioData();
    }
    
    private void loadColegioData(){
        Colegio colegio = new Colegio();
        colegio.setNombre("El colegio del Olimpo");
        
        Integer[] grados = new Integer[]{10, 10, 11, 11};
        String[] salon = new String[]{"A", "B", "A", "B"};
        String[][] asignaturas = new String[][] {
                                                    {"Matemáticas", "Español", "Ingles básico"},
                                                    {"Matemáticas", "Español", "Ingles avanzado"},
                                                    {"Matemáticas", "Pre Icfes"},
                                                    {"Matemáticas", "Pre Icfes"}
                                                };
        String[][] profesoresByAsignatura = new String[][] {
                                                    {"Némesis", "Príapo", "Iris"},
                                                    {"Némesis", "Príapo", "Iris"},
                                                    {"Némesis", "Némesis"},
                                                    {"Némesis", "Némesis"}
                                                };
        
        String[][] estudientesByAsignaturas = new String[][] {
                                                    {"Afrodita", "Apolo", "Ares"},                       
                                                    {"Artemisa", "Atenea", "Dionisio"},                  
                                                    {"Hefesto", "Hera"},                               
                                                    {"Hermes", "Hades", "Poseidon", "Zeus"}
                                                };
        for (int i = 0; i < grados.length; i++) {
            Curso curso = registrarCurso(grados[i], salon[i]);
                curso.setAsignaturas(registrarAsignaturaAndEstudiantes(asignaturas[i], curso, profesoresByAsignatura[i], estudientesByAsignaturas[i]));
            colegio.addCurso(curso);
        }       
        
        colegioService.save(colegio);
    }
    
    private Curso registrarCurso(Integer grado, String salon){
        Curso curso = new Curso();
        curso.setGrado(grado);
        curso.setSalon(salon); 
        return curso;
    }
    
    private List<Asignatura> registrarAsignaturaAndEstudiantes(String[] asignaturas, Curso curso, String[] profesorNombre, String[] estudiantes){
        List<Asignatura> asignaturasList = new ArrayList<>(); 
        List<Estudiante> estudiantesList = new ArrayList<>();
        
        for (int i = 0; i < estudiantes.length; i++) {
            Estudiante estudiante = new Estudiante();
            estudiante.setNombre(estudiantes[i]);
            estudiantesList.add(estudiante);
        }
        
        for (int i = 0; i < asignaturas.length; i++) {
            Asignatura asignatura = new Asignatura();
            asignatura.setNombre(asignaturas[i]);
            asignatura.setCurso(curso);
            asignatura.setProfesor(findProfesorByName(profesorNombre[i]));
            asignaturasList.add(asignatura);
        }
        
        for (int i = 0; i < estudiantesList.size(); i++) {
           estudiantesList.get(i).setAsignaturas(asignaturasList);
        }
        
        for (int i = 0; i < asignaturasList.size(); i++) {
           asignaturasList.get(i).setEstudiantes(estudiantesList);
        }
        
        return asignaturasList;
    }
    
    private Profesor findProfesorByName(String nombre){
        return profesorService.findByNombre(nombre);
    }
}
