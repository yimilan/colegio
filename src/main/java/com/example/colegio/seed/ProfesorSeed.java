/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.colegio.seed;

import com.example.colegio.models.service.ProfesorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import com.example.colegio.models.entity.Profesor;
import com.example.colegio.models.service.IColegioService;
import org.springframework.core.annotation.Order;

/**
 *
 * @author ADMIN
 */

@Order(1)
@Component
public class ProfesorSeed implements CommandLineRunner{
       
    @Autowired
    private ProfesorServiceImpl profesorService;

    @Override
    public void run(String... args) throws Exception {
        loadProfesorData();
    }
    
    private void loadProfesorData(){
        String[] profesores = new String[]{"Némesis", "Príapo", "Iris"};
        for (int i = 0; i < profesores.length; i++) {
            profesorService.save(registrarProfesor(profesores[i]));
        }
    }
    
    private Profesor registrarProfesor(String nombre){
        Profesor profesor = new Profesor();
        profesor.setNombre(nombre);
        return profesor;
    }    
}
