/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.colegio.data.send;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 *
 * @author ADMIN
 */

@Component
public class SendDataProfesor {
    
    private Long idProfesor;
    private String nombreProfesor;
    
    private List<Long> idAsignaturas;
    private List<String> nombreAsignaturas;
    
    private List<Long> idCursos;
    private List<String> nombreCursos;
        
    private List<Long[]> idEstudiantes;
    private List<String[]> nombreEstudiantes;

    
    public void addIdCursos(Long idCursos){
        this.idCursos.add(idCursos);
    }
    
    public void addCursos(String nombreCursos){
        this.nombreCursos.add(nombreCursos);
    }    
    
    public void addIdAsignaturas(Long idAsignaturas){
        this.idAsignaturas.add(idAsignaturas);
    }
    
    public void addNombreAsignaturas(String nombreAsignaturas){
        this.nombreAsignaturas.add(nombreAsignaturas);
    }
    
    public void addIdEstudiantes(Long[] idEstudiantes){
        this.idEstudiantes.add(idEstudiantes);
    }
    
    public void addNombreEstudiantes(String[] nombreEstudiantes){
        this.nombreEstudiantes.add(nombreEstudiantes);
    }
    
    public SendDataProfesor() {
        this.idCursos = new ArrayList<>();
        this.nombreCursos = new ArrayList<>();
        this.idAsignaturas = new ArrayList<>();
        this.nombreAsignaturas = new ArrayList<>();
        this.idEstudiantes = new ArrayList<>();
        this.nombreEstudiantes = new ArrayList<>();
    }

    public Long getIdProfesor() {
        return idProfesor;
    }

    public void setIdProfesor(Long idProfesor) {
        this.idProfesor = idProfesor;
    }

    public String getNombreProfesor() {
        return nombreProfesor;
    }

    public void setNombreProfesor(String nombreProfesor) {
        this.nombreProfesor = nombreProfesor;
    }

    public List<Long> getIdAsignaturas() {
        return idAsignaturas;
    }

    public void setIdAsignaturas(List<Long> idAsignaturas) {
        this.idAsignaturas = idAsignaturas;
    }

    public List<String> getNombreAsignaturas() {
        return nombreAsignaturas;
    }

    public void setNombreAsignaturas(List<String> nombreAsignaturas) {
        this.nombreAsignaturas = nombreAsignaturas;
    }

    public List<Long[]> getIdEstudiantes() {
        return idEstudiantes;
    }

    public void setIdEstudiantes(List<Long[]> idEstudiantes) {
        this.idEstudiantes = idEstudiantes;
    }

    public List<String[]> getNombreEstudiantes() {
        return nombreEstudiantes;
    }

    public void setNombreEstudiantes(List<String[]> nombreEstudiantes) {
        this.nombreEstudiantes = nombreEstudiantes;
    }

    public List<Long> getIdCursos() {
        return idCursos;
    }

    public void setIdCursos(List<Long> idCursos) {
        this.idCursos = idCursos;
    }

    public List<String> getNombreCursos() {
        return nombreCursos;
    }

    public void setNombreCursos(List<String> nombreCursos) {
        this.nombreCursos = nombreCursos;
    }
    
    
    
}
