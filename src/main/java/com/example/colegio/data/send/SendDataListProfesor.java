/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.colegio.data.send;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public class SendDataListProfesor {
    
    private List<Long> idProfesores;
    private List<String> nombreProfesores;

    public SendDataListProfesor() {
        this.idProfesores = new ArrayList<>();
        this.nombreProfesores = new ArrayList<>();
    }
    
    public void addIdProfesores(Long idProfesores){
        this.idProfesores.add(idProfesores);
    }
    
    public void addNombreProfesores(String nombreProfesores){
        this.nombreProfesores.add(nombreProfesores);
    }
    

    public List<Long> getIdProfesores() {
        return idProfesores;
    }

    public void setIdProfesores(List<Long> idProfesores) {
        this.idProfesores = idProfesores;
    }

    public List<String> getNombreProfesores() {
        return nombreProfesores;
    }

    public void setNombreProfesores(List<String> nombreProfesores) {
        this.nombreProfesores = nombreProfesores;
    }
    
    
}
