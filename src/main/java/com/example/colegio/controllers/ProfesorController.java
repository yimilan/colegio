/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.colegio.controllers;

import com.example.colegio.data.send.SendDataListProfesor;
import com.example.colegio.data.send.SendDataProfesor;
import com.example.colegio.models.service.ProfesorServiceImpl;
import com.example.colegio.models.entity.Profesor;
import com.example.colegio.models.entity.Curso;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ADMIN
 */

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class ProfesorController {
    
    @Autowired
    private ProfesorServiceImpl profesorService;
    
    @GetMapping({"/find"})
    public SendDataListProfesor index(Model model)
    {
        SendDataListProfesor dataListProfesor = new SendDataListProfesor();
        List<Profesor> profesores = listarProfesores();
        
        for (int i = 0; i < profesores.size(); i++) {
            dataListProfesor.addIdProfesores(profesores.get(i).getId());
            dataListProfesor.addNombreProfesores(profesores.get(i).getNombre());
        }
        
        return dataListProfesor;
    }
    
    @GetMapping(value = "/find/{val}", produces = {"application/json"})
    public @ResponseBody SendDataProfesor findProfesor(@PathVariable String val){
        Profesor profesor = profesorService.findOne(Long.parseLong(val));
        SendDataProfesor dataProfesor = new SendDataProfesor();
        
        dataProfesor.setIdProfesor(profesor.getId());
        dataProfesor.setNombreProfesor(profesor.getNombre());
        
        for (int i = 0; i < profesor.getAsignaturas().size(); i++) {            
            Long[] estudiantesIds = new Long[profesor.getAsignaturas().get(i).getEstudiantes().size()];
            String[] estudiantesNombres = new String[profesor.getAsignaturas().get(i).getEstudiantes().size()];
            
            dataProfesor.addIdAsignaturas(profesor.getAsignaturas().get(i).getId());
            dataProfesor.addNombreAsignaturas(profesor.getAsignaturas().get(i).getNombre());
            
            dataProfesor.addIdCursos(profesor.getAsignaturas().get(i).getCurso().getId());
            dataProfesor.addCursos(profesor.getAsignaturas().get(i).getCurso().getGrado() + profesor.getAsignaturas().get(i).getCurso().getSalon());
            
            for (int j = 0; j < profesor.getAsignaturas().get(i).getEstudiantes().size(); j++) {
                estudiantesIds[j] = profesor.getAsignaturas().get(i).getEstudiantes().get(j).getId();
                estudiantesNombres[j] = profesor.getAsignaturas().get(i).getEstudiantes().get(j).getNombre();
            }
            
            dataProfesor.addIdEstudiantes(estudiantesIds);
            dataProfesor.addNombreEstudiantes(estudiantesNombres);
        }
        System.out.println("hola");
        return dataProfesor;
    }
    
    public List<Profesor> listarProfesores(){
        return profesorService.findAll();
    }
}
