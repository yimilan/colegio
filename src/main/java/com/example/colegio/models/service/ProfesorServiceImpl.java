/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.colegio.models.service;

import com.example.colegio.models.dao.IProfesorDao;
import com.example.colegio.models.entity.Profesor;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ADMIN
 */
@Service
public class ProfesorServiceImpl implements IProfesorService{
    
    @Autowired
    private IProfesorDao profesorDao;

    @Override
    @Transactional(readOnly = true)
    public List<Profesor> findAll() {
        return (List<Profesor>) profesorDao.findAll();
    }

    @Override
    @Transactional
    public void save(Profesor asignatura) {
        profesorDao.save(asignatura);
    }

    @Override
    @Transactional(readOnly = true)
    public Profesor findOne(Long id) {
        return profesorDao.findById(id).orElse(null);
    }

    @Override
    public void delete(Long id) {
        profesorDao.deleteById(id);
    }

    @Override
    public Profesor findByNombre(String nombre) {
        return profesorDao.findByNombre(nombre);
    }
}
