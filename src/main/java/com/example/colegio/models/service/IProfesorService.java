/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.colegio.models.service;

import com.example.colegio.models.entity.Profesor;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface IProfesorService {
    
    public List<Profesor> findAll();
    
    public void save(Profesor profesor);
    
    public Profesor findOne(Long id);
    
    public void delete(Long id);
    
    public Profesor findByNombre(String nombre);
}
