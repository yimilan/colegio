/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.colegio.models.service;

import java.util.List;
import com.example.colegio.models.entity.Colegio;

/**
 *
 * @author ADMIN
 */
public interface IColegioService {
    
    public List<Colegio> findAll();
    
    public void save(Colegio colegio);
    
    public Colegio findOne(Long id);
    
    public void delete(Long id);
    
}
