/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.colegio.models.service;

import com.example.colegio.models.dao.IColegioDao;
import com.example.colegio.models.entity.Colegio;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ADMIN
 */
@Service
public class ColegioServiceImpl implements IColegioService{

    @Autowired
    private IColegioDao colegioDao;
    
    
    @Override
    @Transactional(readOnly = true)
    public List<Colegio> findAll() {
        return (List<Colegio>) colegioDao.findAll();
    }

    @Override
    @Transactional
    public void save(Colegio colegio) {
        colegioDao.save(colegio);
    }

    @Override
    @Transactional(readOnly = true)
    public Colegio findOne(Long id) {
        return colegioDao.findById(id).orElse(null);
    }

    @Override
    public void delete(Long id) {
        colegioDao.deleteById(id);
    }
    
}
