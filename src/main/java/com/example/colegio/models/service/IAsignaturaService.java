/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.colegio.models.service;

import com.example.colegio.models.entity.Asignatura;
import com.example.colegio.models.entity.Curso;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface IAsignaturaService {
    
    public List<Asignatura> findAll();
    
    public void save(Asignatura asignatura);
    
    public Asignatura findOne(Long id);
    
    public void delete(Long id);
    
    public Asignatura nombreAndCurso(String nombre, Long cursoId);
}
