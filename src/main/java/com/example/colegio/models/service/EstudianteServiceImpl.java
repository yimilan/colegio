/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.colegio.models.service;

import com.example.colegio.models.dao.IEstudianteDao;
import com.example.colegio.models.entity.Estudiante;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ADMIN
 */
@Service
public class EstudianteServiceImpl implements IEstudianteService{
    
    @Autowired
    private IEstudianteDao estudianteDao;

    @Override
    @Transactional(readOnly = true)
    public List<Estudiante> findAll() {
        return (List<Estudiante>) estudianteDao.findAll();
    }

    @Override
    @Transactional
    public void save(Estudiante asignatura) {
        estudianteDao.save(asignatura);
    }

    @Override
    @Transactional(readOnly = true)
    public Estudiante findOne(Long id) {
        return estudianteDao.findById(id).orElse(null);
    }

    @Override
    public void delete(Long id) {
        estudianteDao.deleteById(id);
    }
    
    @Override
    public Estudiante findByNombre(String nombre) {
        return estudianteDao.findByNombre(nombre);
    }
}
