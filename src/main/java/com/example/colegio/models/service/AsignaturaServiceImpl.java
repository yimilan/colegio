/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.colegio.models.service;

import com.example.colegio.models.dao.IAsignaturaDao;
import com.example.colegio.models.entity.Asignatura;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.example.colegio.models.entity.Curso;
import org.springframework.stereotype.Service;

/**
 *
 * @author ADMIN
 */
@Service
public class AsignaturaServiceImpl implements IAsignaturaService{
    
    @Autowired
    private IAsignaturaDao asignaturaDao;

    @Override
    @Transactional(readOnly = true)
    public List<Asignatura> findAll() {
        return (List<Asignatura>) asignaturaDao.findAll();
    }

    @Override
    @Transactional
    public void save(Asignatura asignatura) {
        asignaturaDao.save(asignatura);
    }

    @Override
    @Transactional(readOnly = true)
    public Asignatura findOne(Long id) {
        return asignaturaDao.findById(id).orElse(null);
    }

    @Override
    public void delete(Long id) {
        asignaturaDao.deleteById(id);
    }

    @Override
    public Asignatura nombreAndCurso(String nombre, Long cursoId) {
        return asignaturaDao.nombreAndCurso(nombre, cursoId);
    }
}
