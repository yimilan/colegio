/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.colegio.models.service;

import com.example.colegio.models.dao.ICursoDao;
import com.example.colegio.models.entity.Curso;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ADMIN
 */
@Service
public class CursoServiceImpl implements ICursoService{

    @Autowired
    private ICursoDao cursoDao;
    
    
    @Override
    @Transactional(readOnly = true)
    public List<Curso> findAll() {
        return (List<Curso>) cursoDao.findAll();
    }

    @Override
    @Transactional
    public void save(Curso colegio) {
        cursoDao.save(colegio);
    }

    @Override
    @Transactional(readOnly = true)
    public Curso findOne(Long id) {
        return cursoDao.findById(id).orElse(null);
    }

    @Override
    public void delete(Long id) {
        cursoDao.deleteById(id);
    }

    @Override
    public Curso findByGradoAndSalon(Integer grado, String salon) {
        return cursoDao.findByGradoAndSalon(grado, salon);
    }
    
    
}
