/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.colegio.models.service;

import com.example.colegio.models.entity.Curso;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface ICursoService {
    
    public List<Curso> findAll();
    
    public void save(Curso curso);
    
    public Curso findOne(Long id);
    
    public void delete(Long id);
    
    Curso findByGradoAndSalon(Integer grado, String salon);
    
}
