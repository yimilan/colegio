/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.colegio.models.service;

import com.example.colegio.models.entity.Estudiante;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public interface IEstudianteService {
    
    public List<Estudiante> findAll();
    
    public void save(Estudiante estudiante);
    
    public Estudiante findOne(Long id);
    
    public void delete(Long id);
    
    Estudiante findByNombre(String nombre);
}
