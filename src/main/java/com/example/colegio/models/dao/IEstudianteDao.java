/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.colegio.models.dao;

import com.example.colegio.models.entity.Estudiante;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author ADMIN
 */
public interface IEstudianteDao extends CrudRepository<Estudiante, Long>{
    Estudiante findByNombre(String nombre);
}
