/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.colegio.models.dao;

import com.example.colegio.models.entity.Curso;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author ADMIN
 */
public interface ICursoDao extends CrudRepository<Curso, Long>{
    
    /**
     *
     * @param grado
     * @param salon
     * @return Curso
     */
    Curso findByGradoAndSalon(Integer grado, String salon);
}
