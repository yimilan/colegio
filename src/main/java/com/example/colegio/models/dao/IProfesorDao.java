/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.colegio.models.dao;

import com.example.colegio.models.entity.Profesor;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author ADMIN
 */
public interface IProfesorDao extends CrudRepository<Profesor, Long>{
    Profesor findByNombre(String nombre);
}
