/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.colegio.models.dao;

import com.example.colegio.models.entity.Asignatura;
import com.example.colegio.models.entity.Curso;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author ADMIN
 */
public interface IAsignaturaDao extends CrudRepository<Asignatura, Long>{
    
    @Query(value = "SELECT * FROM ASIGNATURAS WHERE NOMBRE = :nombre AND CURSO_ID = :cursoId", nativeQuery = true)
    Asignatura nombreAndCurso(@Param("nombre") String nombre, @Param("cursoId") Long cursoId);
}
